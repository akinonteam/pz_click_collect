# Click and Collect

This app enables retail store delivery option.

## First step

This app uses gettext for translation. 
If there is no **gettext** function in the project, you should define this function as follows to avoid any errors:

    window.gettext = (str) =>  str;

## Installation

Install the pz-click-collect package:

    yarn add git+ssh://git@bitbucket.org:akinonteam/pz_click_collect.git#db18a09

Install the related shop-packages version:

    yarn add git+ssh://git@bitbucket.org:akinonteam/shop-packages.git#9fb5886231a6da86bbc4e3d1fc13032ec881943b

## Usage

In order to complete integration,

Import PzClickCollect:

    import PzClickCollect from 'pz-click-collect';

Create a new instance:

    new PzClickCollect();

Import styles:

    @import '~pz-click-collect';

## Options

Default options:

    new PzClickCollect({
	    deliveryStoreLabel: 'Teslimat Mağazası',
	    boxClass: 'click-collect',
	    buttonIcon: null, // pz-icon-font name
	    buttonText: 'Mağazadan Teslim Al',
	    addressBoxSelector: '.js-address-box',
	    newAddressButtonSelector: '.js-new-address',
	    deleteAddressButtonSelector: '.js-delete-address',
	    updateAddressButtonSelector: '.js-update-address',
	    shippingAddressListSelector: '#CheckoutAddressList',
	    billingAddressListSelector: '#CheckoutBillingList',
	    checkoutProceedButtonSelector: '#CheckoutProceedButton',
	    sameBillingCheckboxSelector: '#CheckoutSameBillingCheck',
	})
